#define _GNU_SOURCE
#include <sched.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <libnio.h>

bool manual = true;

void
set_cpu(int cpu_id)
{
	cpu_set_t set;
	CPU_ZERO(&set);
	CPU_SET(cpu_id, &set);
	sched_setaffinity(0, sizeof(cpu_set_t), &set);

	assert(cpu_id == sched_getcpu());
}

void *
watching(void *arg)
{
	int cpu_id = *(int *)arg;
	set_cpu(cpu_id);
	printf("Watcher thread running on CPU: %d\n", sched_getcpu());

	while (1) {
		sleep(1);
	}

	return NULL;
}
#define MMAP_LENGTH 1024

static void *
backend_mmap(const char *type, size_t size, int socket)
{
	printf("type: %s, size: %zu, socket: %d\n", type, size, socket);
	char *data = mmap(NULL, size, PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	return data;
}

static void
backend_munmap(void *obj)
{
	if (obj != NULL) {
		munmap(obj, MMAP_LENGTH);
	}
}

static size_t
backend_size(void *obj)
{
	return 0;
}

int
main(void)
{
	// Pin main thread on socket 0
	set_cpu(50);
	printf("Main thread running on CPU: %d\n", sched_getcpu());

	nio_alloc_set(backend_mmap, backend_munmap, NULL, NULL, backend_size, NULL, NIO_MALLOC_SOCKET_LOCAL, 4);
	NIO_MALLOC_LOCAL(NIO_MALLOC_OPTION_DISABLE_STATS, NULL);

	nio_ring_t *ring = nio_ring_new("shared_ring", 256, 16, 0, 0);
	printf("ring: %p\n", ring);

	// Create a few passive watchers and pin them on some CPUs on other sockets
	// These watchers' CPUs will receive TLB shootdown IPIs
	int cpu_ids[] = {63, 75, 90};
	int num_threads = sizeof(cpu_ids) / sizeof(cpu_ids[0]);
	pthread_t watchers[num_threads];
	for (int i = 0; i < num_threads; i++) {
		pthread_create(&watchers[i], NULL, watching, &cpu_ids[i]);
	}

	sleep(1);

	void *temp = nio_malloc(MMAP_LENGTH);
	nio_free(temp);

	printf("ENTER to start\n");
	getchar();
	printf("Running...\nCtrl-C to quit\n");

	// Mmap a random file
	int fd = open("/home/tranbaolong/rbuild/temp", O_RDWR, 0600);
	struct stat s;
	fstat(fd, &s);
	int length = s.st_size;
	printf("length=%d\n", length);

	char *data = mmap(0, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (data == MAP_FAILED) {
		printf("mmap failed");
		return -1;
	}

	// touch the page
	data[1] = 'a';
	sleep(1);

	pid_t me = getpid();
	printf("Me: %d\n", me);

	pid_t child = 0;
	// Uncomment to fork. Or not. Whatever.
	child = fork();
	if (child == 0) {
		printf("Child: %d\n", getpid());

		if (manual) {
			printf("Enter to unmap\n");
			getchar();

		}

		data[1] = 'b';
		msync(data, length, MS_SYNC);
		sleep(1);

		munmap(data, length);
		printf("unmapped\n");
		printf("Waiting...\n");
		getchar();
		// close(fd);
	}

	// close(fd);

	for (int i = 0; i < num_threads; i++) {
		pthread_join(watchers[i], NULL);
	}

	return 0;
}
